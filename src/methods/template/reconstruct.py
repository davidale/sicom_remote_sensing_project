"""The main file for the reconstruction.
This file should NOT be modified except the body of the 'run_reconstruction' function.
Students can call their functions (declared in others files of src/methods/your_name).
"""


import numpy as np


def run_reconstruction(pan: np.ndarray, ms1: np.ndarray, ms2: np.ndarray) -> np.ndarray:
    """Performs ata fusion of the three acquisitions.

    Args:
        pan (np.ndarray): Panchromatic image.
        ms1 (np.ndarray): Mid resolution multispectral image.
        ms2 (np.ndarray): Low resolution multispectral image.

    Returns:
        np.ndarray: Full spatial and spectral image.
    """
    # Performing the reconstruction.
    #T0D0

    result = np.zeros((pan.shape[0], pan.shape[1], ms1.shape[2]))
    for i in range(ms1.shape[2]):
        result[:, :, i] = pan
    return result


####
####
####

####      ####                ####        #############
####      ######              ####      ##################
####      ########            ####      ####################
####      ##########          ####      ####        ########
####      ############        ####      ####            ####
####      ####  ########      ####      ####            ####
####      ####    ########    ####      ####            ####
####      ####      ########  ####      ####            ####
####      ####  ##    ######  ####      ####          ######
####      ####  ####      ##  ####      ####    ############
####      ####  ######        ####      ####    ##########
####      ####  ##########    ####      ####    ########
####      ####      ########  ####      ####
####      ####        ############      ####
####      ####          ##########      ####
####      ####            ########      ####
####      ####              ######      ####

# 2023
# Authors: Mauro Dalla Mura and Matthieu Muller
